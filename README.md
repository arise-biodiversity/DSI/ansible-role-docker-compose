# ansible-docker-compose

Deploy one or more docker-compose projects and configure based on .env files.

## Requirements

This role is tested on Ubuntu 20.04 and 22.04 with Ansible 4.7.0 / Core 2.11.
In the _requirments.yml_ you have to add:
```
  collections:
    - name: community.docker
      version: 3.10.4
```

## Example Playbook

## Role Variables

This role assumes the following variable structure, with a Traefik compose
config as example:

```yaml
docker_compose_projects:
  - name: openstackproxy
    repo: https://gitlab.com/naturalis/core/openstack/docker-compose-traefik-openstack
    repo_version: develop # Can be a tag, branch or commit
    env:
      TRAEFIK_VERSION: v2.2.8
      TRAEFIK_LOG_LEVEL: DEBUG
      TRAEFIK_API_ENABLED: "true"
      TRAEFIK_API_INSECURE: "true"
```

It is also possible to have a local docker-compose folder containing all needed files for a specific docker-compose folder. This is mutually exclusive with the `repo:` setting.

The example has a files/docker-compose-openstackproxy folder in ./playbooks

```yaml
docker_compose_projects:
  - name: openstackproxy-keeper
    folder: files/docker-compose-openstackproxy
    env:
      TRAEFIK_VERSION: v2.2.8
      TRAEFIK_LOG_LEVEL: DEBUG
      TRAEFIK_API_ENABLED: "true"
      TRAEFIK_API_INSECURE: "true"
```
By default '.env' and the 'letsencrypt' subdirectory are _not_ synced. Because they can change. If there are more files or subfolders you don't want to be synced, use this:
```
# N.B. Default are first 3 options
docker_compose_rsync_opts:
  - "--exclude=.env"
  - "--exclude=letsencrypt"
  - "--rsh='/usr/bin/ssh -S none -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o loglevel=ERROR'"
  - "--exclude=IDONTWANTTHISSUBFOLDERTOBESYNCED"
```

If you want to only start some of the services defined in a docker-compose file
use the following statement:

```yaml
docker_compose_projects:
  - name: openstackproxy
    repo: https://gitlab.com/naturalis/core/openstack/docker-compose-traefik-openstack
    repo_version: develop # Can be a tag, branch or commit
    services:
      - traefik
      - prometheus
    env:
      TRAEFIK_VERSION: v2.2.8
```

If you want to use multiple overlapping docker-compose definitions you can use
the following statement. Please note that the order of files listed matters.

```yaml
docker_compose_projects:
  - name: openstackproxy
    repo: https://gitlab.com/naturalis/core/openstack/docker-compose-traefik-openstack
    repo_version: develop # Can be a tag, branch or commit
    files:
      - docker-compose.yml
      - docker-compose.deploy.yml
    env:
      TRAEFIK_VERSION: v2.2.8
```

If you omit the `docker_compose_projects` variable the role will just install
Docker, docker-compose and related packages and skip further configuration.

Apart from that these generic variables need to be set as well:

```yaml
env: environmentname # Defaults to 'development'
service: servicename # Defaults to the name of the compose project
```

When using Traefik for certificate handling, please provide the following:
```yaml
traefik_aws_access_key_id: traefik_access_key_id_for_route53
traefik_aws_secret_access_key: traefik_secret_access_key_for_route53
```

When containers in a private registry are used this role allows you to login to
the registry; also when the code repo is private:

```yaml
docker_registry_login: true
docker_registry_url: registry.gitlab.com
docker_registry_user: <gitlab+deply-token-***>
docker_registry_secret: "{{ secrets_group.production.docker_registry_secret }}"
```

To make such a deploy token, go to the project level, select
_Settings/Repository/Deploy tokens_, and than make a deploy-token with the
scope of `read_repository` and/or `read_registry`.

Set `docker_compose_force_pull` to `true` if you want to pull docker images on
every ansible run. This can be useful when you update docker images, but keep
the same tag in your docker-compose. When a new image is detected, this play
reports a change.

```yaml
docker_compose_force_pull: false #default setting
```

Do you want to checkout the whole repo, you need to specify that private repo as follows:

```yaml  
  docker_compose_projects:
    - name: nummerreeks
      repo: https://{{ docker_registry_user }}:{{ docker_registry_secret }}@gitlab.com/naturalis/bii/nummerreeks/docker-compose-nummerreeks.git
```

## Dependencies

As part of this role Docker and docker-compose will be installed. The
installation of Docker is based on the nickjj/ansible-docker role.

In this example the docker-compose role is applied and in a separate task an
extra config file is generated that can be used for consumption in a container.
The containers defined in the docker-compose will get started by a handler
after the template has been generated.

```yaml
---
- name: Deploy reverse proxies
  hosts: reverseproxies
  gather_facts: true
  become: true
  tasks:
    - name: Apply docker-compose role
      include_role:
        name: docker-compose
        apply:
          tags: docker-compose
      tags: always

    - name: Add extra config for Traefik
      template:
        src: traefik-config.toml.j2
        dest: /opt/compose_projects/openstackproxy/config/traefik-config.toml
...
```
## Remarks
A `docker compose restart (<service>)` restarts the containers, it does not implement new variables from _.env_.
A `docker compose up -d` does effectuate those changed environment variables, and only restarts those containers that are affected.
